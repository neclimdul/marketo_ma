<?php

namespace Drupal\Tests\marketo_ma\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\marketo_ma\Traits\MarketoMaFunctionalHelperTrait;

/**
 * Base for Marketo MA functional javascript tests.
 *
 * @group marketo_ma-js
 */
abstract class MarketoMaJavascriptTestBase extends WebDriverTestBase {

  use MarketoMaFunctionalHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'encryption',
    'marketo_ma',
    'marketo_mock_client',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockConfig();
  }

}
