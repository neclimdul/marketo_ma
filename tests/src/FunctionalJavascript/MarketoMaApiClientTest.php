<?php

namespace Drupal\Tests\marketo_ma\FunctionalJavascript;

/**
 * Tests the Marketo MA module in API Client mode.
 *
 * @group marketo_ma-js
 */
class MarketoMaApiClientTest extends MarketoMaJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up required settings.
    $this->config
      ->set('tracking_method', 'api_client')
      ->save();
  }

  /**
   * Tests that munchkin tracking works with API tracking?
   */
  public function testMunchkinLeadAssociation() {
    $marketo_user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);

    $this->drupalGet('<front>');
    $this->assertMunchkinTracking();

    // Log into drupal.
    $this->drupalLogin($marketo_user);
    $this->assertMunchkinTracking();

    // Get an un-tracked page. (user/*/* not tracked by default)
    $this->drupalGet('/user/' . $marketo_user->id() . '/edit');
    $this->assertNoMunchkinTracking();

    // Get another un-tracked page.
    $this->drupalGet('/admin/');
    $this->assertNoMunchkinTracking();
  }

}
