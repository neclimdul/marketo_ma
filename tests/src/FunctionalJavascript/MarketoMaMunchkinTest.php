<?php

namespace Drupal\Tests\marketo_ma\FunctionalJavascript;

use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Tests the Marketo MA module in Munchkin mode.
 *
 * @group marketo_ma-js
 */
class MarketoMaMunchkinTest extends MarketoMaJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'filter'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up required settings.
    $this->config
      ->set('tracking_method', MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN)
      ->save();
  }

  /**
   * Tests if a lead is associated when the user logs in.
   */
  public function testMunchkinLeadAssociation() {
    $marketo_user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $node = $this->drupalCreateNode(['type' => 'page', 'title' => 'test']);

    // Frontpage should be tracked.
    $this->drupalGet('<front>');
    // This seems like a bug...
    $this->assertMunchkinTracking();

    // Misc nodes should be trackerd.
    $this->drupalGet('/node/' . $node->id());
    $this->assertMunchkinTracking();

    // Log into drupal.
    $this->drupalLogin($marketo_user);
    $this->assertMunchkinTracking();

    // Get a tracked page.
    $this->drupalGet('/node/' . $node->id());
    $this->assertMunchkinTracking();
    $this->drupalGet('/user/password');
    $this->assertMunchkinTracking();

    // Get an un-tracked page. (user/*/* not tracked by default)
    $this->drupalGet('/user/' . $marketo_user->id() . '/edit');
    $this->assertNoMunchkinTracking();

    // Get another un-tracked page.
    $this->drupalGet('/admin/');
    $this->assertNoMunchkinTracking();
  }

}
