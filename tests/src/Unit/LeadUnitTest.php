<?php

namespace Drupal\Tests\marketo_ma\Unit;

use Drupal\marketo_ma\Lead;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\marketo_ma\Lead
 *
 * @group marketo_ma
 */
class LeadUnitTest extends UnitTestCase {

  /**
   * Lead data.
   *
   * @var string[]
   */
  public $leadData;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->leadData = [
      'id' => '12',
      'email' => 'mail@example.org',
      'firstName' => 'Fancy first name',
      'lastName' => 'Fancy last name',
      'cookies' => 'ILoveCookies',
    ];

  }

  /**
   * @covers ::id
   * @covers ::getEmail
   * @covers ::getCookie
   * @covers ::set
   * @covers ::data
   */
  public function testLead() {
    $lead = new Lead($this->leadData);

    $this->assertEquals($this->leadData['id'], $lead->id());
    $this->assertEquals($this->leadData['email'], $lead->getEmail());
    $this->assertNull($lead->getCookie());

    $new_lead_data = ['id' => '13'] + $this->leadData;
    $this->assertEquals($new_lead_data, $lead->set('id', '13')->data());

    $this->assertEquals('Cookies!!!!!', $lead->setCookie('Cookies!!!!!')->getCookie());
  }

  /**
   * @covers ::__construct
   */
  public function testSerialization() {
    $lead = new Lead($this->leadData);
    // @codingStandardsIgnoreLine
    $this->assertEquals($lead, unserialize(serialize($lead)));
  }

}
