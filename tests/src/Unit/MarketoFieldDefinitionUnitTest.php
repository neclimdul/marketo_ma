<?php

namespace Drupal\Tests\marketo_ma\Unit;

use Drupal\marketo_ma\MarketoFieldDefinition;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\marketo_ma\MarketoFieldDefinition
 *
 * @group marketo_ma
 */
class MarketoFieldDefinitionUnitTest extends UnitTestCase {

  /**
   * Sample field data.
   *
   * @var array
   */
  protected $fieldData = [
    'id' => 1,
    'displayName' => 'First Name',
    'dataType' => 'string',
    'restName' => 'firstName',
    'restReadOnly' => FALSE,
    'soapName' => 'firstname',
    'soapReadOnly' => FALSE,
  ];

  /**
   * @covers ::__construct
   */
  public function testSerialization() {
    $field = new MarketoFieldDefinition($this->fieldData);
    // @codingStandardsIgnoreLine
    $this->assertEquals($field, unserialize(serialize($field)));
  }

  /**
   * @covers ::id
   */
  public function testId() {
    $field = new MarketoFieldDefinition($this->fieldData);
    $this->assertEquals($this->fieldData['id'], $field->id());
  }

  /**
   * @covers ::getDisplayName
   */
  public function testDisplayName() {
    $field = new MarketoFieldDefinition($this->fieldData);
    $this->assertEquals($this->fieldData['displayName'], $field->getDisplayName());
  }

  /**
   * @covers ::getFieldName
   */
  public function testFieldName() {
    $field = new MarketoFieldDefinition($this->fieldData);

    $this->assertEquals($this->fieldData['restName'], $field->getFieldName(MarketoMaServiceInterface::TRACKING_METHOD_API));
    $this->assertEquals($this->fieldData['soapName'], $field->getFieldName(MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN));
  }

}
