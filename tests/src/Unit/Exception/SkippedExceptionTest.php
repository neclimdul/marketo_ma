<?php

namespace Drupal\Tests\marketo_ma\Unit\Exception;

use Drupal\marketo_ma\Exception\SkippedException;
use Drupal\marketo_ma\Exception\SkippedReason;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\marketo_ma\Exception\SkippedException
 */
class SkippedExceptionTest extends UnitTestCase {

  /**
   * @covers ::__construct
   * @covers ::setMessage
   */
  public function testExtended() {
    $this->assertSame(123, (new SkippedException([], 'foo', 123))->getCode());
    $this->assertSame("Something went wrong with your request: []", (new SkippedException([], '', 123))->getMessage());
    $this->assertSame("foo\nSomething went wrong with your request: []", (new SkippedException([], 'foo', 123))->getMessage());
    // This is wrong...
    $this->assertSame("foo\nSomething went wrong with your request: [{\"code\":\"1001\",\"reason\":\"bad field value\"}]", (new SkippedException([
      new SkippedReason(1001, 'bad field value'),
    ], 'foo', 123))->getMessage());
  }

  /**
   * @covers ::containsCode
   */
  public function testContainsCode() {
    $reason = new SkippedReason(1001, 'bad field value');
    $exception = new SkippedException([
      $reason,
    ], 'foo', 123);
    $this->assertSame($reason, $exception->containsCode(1001));
    $this->assertFalse($exception->containsCode(1002));
  }

}
