<?php

namespace Drupal\Tests\marketo_ma\Kernel;

use Drupal\marketo_ma\Service\MarketoMaMunchkinInterface;

/**
 * Tests the Marketo MA Munchkin service.
 *
 * @group marketo_ma
 */
class MarketoMaMunchkinServiceTest extends MarketoMaKernelTestBase {

  /**
   * The marketo_ma munchkin service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaMunchkinInterface
   */
  protected $munchkin;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up required settings.
    $this->config
      ->set('tracking_method', 'munchkin')
      ->save();

    // Get the API client service.
    $this->munchkin = \Drupal::service('marketo_ma.munchkin');
  }

  /**
   * Tests the marketo_ma service.
   */
  public function testMarketoMaService() {
    $this->assertTrue($this->munchkin instanceof MarketoMaMunchkinInterface);
    $this->markTestIncomplete('This does not really work without connecting our test to a live marketo instance.');
    $this->assertTrue($this->munchkin->isConfigured());
    $this->assertEquals(getenv('marketo_ma_munchkin_account_id'), $this->munchkin->getAccountId());
  }

}
