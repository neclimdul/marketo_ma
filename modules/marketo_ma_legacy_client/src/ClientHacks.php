<?php

namespace Drupal\marketo_ma_legacy_client;

use CSD\Marketo\Client;

/**
 * Any local hacks needed to keep this running.
 *
 * Expose some methods that are hidden in service.json.
 *
 * @method \CSD\Marketo\Response|string getActivityTypes()
 */
class ClientHacks extends Client {

}
