<?php

namespace Drupal\marketo_ma_user\Service;

use Drupal\user\UserInterface;

/**
 * User integration service.
 */
interface MarketoMaUserServiceInterface {

  // The Marketo MA User module config name.
  const MARKETO_MA_USER_CONFIG_NAME = 'marketo_ma_user.settings';

  /**
   * Gets the Marketo MA user config object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   *   The `marketo_ma_user.settings` config object.
   */
  public function config();

  /**
   * Callback for `hook_user_login`.
   *
   * @param \Drupal\user\UserInterface $account
   *   A Drupal user logging in.
   */
  public function userLogin(UserInterface $account);

  /**
   * Callback for `hook_entity_create`.
   *
   * @param \Drupal\user\UserInterface $user
   *   A Drupal user being created.
   */
  public function userCreate(UserInterface $user);

  /**
   * Callback for `hook_entity_update`.
   *
   * @param \Drupal\user\UserInterface $user
   *   A Drupal user being updated.
   */
  public function userUpdate(UserInterface $user);

  /**
   * Update a marketo lead from a Drupal user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user to sync to marketo.
   * @param bool $sync_cookie
   *   Use cookie to sync user session with lead.
   *
   * @return int|null
   *   Lead id of the affected lead.
   */
  public function updateLead(UserInterface $user, bool $sync_cookie = FALSE): ?int;

  /**
   * Get activity types that are defined in marketo.
   *
   * @param bool $reset
   *   Whether to try to refresh the list form the API client.
   *
   * @return \Drupal\marketo_ma\ActivityType[]
   *   All marketo activity types keyed by the marketo activity ID.
   */
  public function getMarketoActivities($reset = FALSE);

}
