<?php

namespace Drupal\marketo_ma_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * User lead data controller.
 */
class MarketoMaUserLeadDataController extends ControllerBase {

  /**
   * The Marketo API client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * The Marketo user service.
   *
   * @var \Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface
   */
  protected $marketoMaUserService;

  /**
   * Construct the lead data controller.
   */
  public function __construct(EntityTypeManager $entity_type_manager, MarketoMaApiClientInterface $api_client, MarketoMaUserServiceInterface $marketo_ma_user_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->marketoMaApiClient = $api_client;
    $this->marketoMaUserService = $marketo_ma_user_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('marketo_ma.api_client'),
      $container->get('marketo_ma.user')
    );
  }

  /**
   * Title callback for the lead page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function viewLeadTitle(UserInterface $user) {
    return $this->t('Marketo Lead (:username)', [':username' => $user->getAccountName()]);
  }

  /**
   * Controller callback to view Marketo lead data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   A table render array of activity for the user.
   */
  public function viewLead(UserInterface $user) {
    // Get the lead from the user's email address.
    $lead = $this->marketoMaApiClient->getLeadByEmail($user->getEmail());
    $header = [
      $this->t('Marketo field name'),
      $this->t('Submitted information'),
    ];
    // Convert the lead data to table rows.
    $rows = !$lead ? [] : array_map(function ($field_value, $field_name) {
      return [
        $this->t(':value', [':value' => $field_name]),
        $this->t(':value', [':value' => $field_value]),
      ];
    }, $lead->data(), array_keys($lead->data()));

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No lead information found for %username.', ['%username' => $user->getEmail()]),
    ];
  }

  /**
   * Title callback for the lead activity page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function viewActivityTitle(UserInterface $user) {
    return $this->t('Marketo Lead Activity (:username)', [':username' => $user->getAccountName()]);
  }

  /**
   * Controller callback to view lead activity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   A table render array of activity for the user.
   */
  public function viewActivity(UserInterface $user) {
    // Get the lead from the user's email address.
    $lead = $this->marketoMaApiClient->getLeadByEmail($user->getEmail());
    // Get the enabled activities.
    $enabled_activities = array_values($this->marketoMaUserService->config()->get('enabled_activities'));
    // Create the headers row.
    $header = [
      $this->t('Marketo activity ID'),
      $this->t('Date/Time'),
      $this->t('Activity Type'),
      $this->t('Asset Name'),
    ];
    if (empty($enabled_activities)) {
      $empty = $this->t('Unable to retrieve. No enabled activity types enabled.');
      $rows = [];
    }
    else {
      $empty = $this->t('No lead activity found for %username.', ['%username' => $user->getAccountName()]);
      $activity = $this->marketoMaApiClient->getLeadActivity($lead, $enabled_activities);
      // Convert the lead data to table rows.
      $rows = array_map(function ($activity) {
        return [
          $activity['id'],
          // @todo convert to site date format?
          $activity['activityDate'],
          // @todo Convert to something readable?
          $activity['activityTypeId'],
          $activity['primaryAttributeValue'],
        ];
      }, $activity);
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#caption' => 'Some recent activities.',
      '#rows' => $rows,
      '#empty' => $empty,
    ];
  }

}
