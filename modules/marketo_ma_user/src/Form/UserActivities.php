<?php

namespace Drupal\marketo_ma_user\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Marketo user activities configuration form.
 */
class UserActivities extends ConfigFormBase {

  /**
   * The Marketo MA API client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The Marketo MA API client.
   *
   * @var \Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface
   */
  protected $marketoMaUserService;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a \Drupal\marketo_ma\Form\MarketoMaSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA API client.
   * @param \Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface $marketo_ma_user_service
   *   The Marketo MA API client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MarketoMaServiceInterface $marketo_ma_service,
    MarketoMaUserServiceInterface $marketo_ma_user_service
  ) {
    parent::__construct($config_factory);
    $this->marketoMaService = $marketo_ma_service;
    $this->marketoMaUserService = $marketo_ma_user_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('marketo_ma'),
      $container->get('marketo_ma.user')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var array $form */
    $form = parent::buildForm($form, $form_state);

    // Get the configuration.
    $config = $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME);

    // @todo ???
    // 'Note: Activities are retrieved by type in batches of 10 types. Each
    // multiple of 10 activity types will require another API request and will
    // affect their performance of activity pages.'
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Enable activities here to have them show up on the user Activities tab.') . '</p>',
    ];

    $form['enabled_activities'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Enabled Activities'),
      '#header' => [
        $this->t('ID'),
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Primary attribute name'),
      ],
      '#options' => $this->getActivityOptions(),
      '#empty' => $this->t('No activity types, try retrieving from marketo.'),
      '#prefix' => '<div id="marketo-enabled-activities-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('enabled_activities'),
    ];
    // Add the ajax button that get's fields from the marketo API.
    $form['actions']['activity_api_retrieve_activities'] = [
      '#type' => 'button',
      '#value' => $this->t('Fetch from Marketo'),
      '#disabled' => !$this->marketoMaService->apiClientCanConnect(),
      '#ajax' => [
        'callback' => [$this, 'retrieveApiActivities'],
        'event' => 'mouseup',
        'wrapper' => 'marketo-enabled-activities-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Retrieving fields from Marketo...'),
        ],
      ],
    ];

    // Add the validation and submit callbacks.
    $form['#validate'][] = [$this, 'validateForm'];
    $form['#submit'][] = [$this, 'submitForm'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME)
      ->set('enabled_activities', array_filter($form_state->getValue('enabled_activities')))
      ->save();
  }

  /**
   * Connects to Marketo and retrieves the API activity types.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form element to replace in the ajax wrapper setting.
   */
  public function retrieveApiActivities(array &$form, FormStateInterface $form_state) {
    $this->marketoMaUserService->getMarketoActivities(TRUE);
    $form['enabled_activities']['#options'] = $this->getActivityOptions();
    return $form['enabled_activities'];
  }

  /**
   * Build the options list for the activities tableselect.
   *
   * @return string[][]
   *   Table column values.
   */
  private function getActivityOptions() {
    return array_map(function ($item) {
      return [
        $this->t(':value', [':value' => $item->id()]),
        $this->t(':value', [':value' => $item->getName()]),
        $this->t(':value', [':value' => $item->getDescription()]),
        $this->t(':value', [':value' => $item->getPrimaryAttributeName()]),
      ];
    }, $this->marketoMaUserService->getMarketoActivities());
  }

}
