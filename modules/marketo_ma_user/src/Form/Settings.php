<?php

namespace Drupal\marketo_ma_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;

/**
 * Marketo user settings form.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var array $form */
    $form = parent::buildForm($form, $form_state);

    // Get the configuration.
    $config = $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME);

    $form['group_events'] = [
      '#title' => $this->t('Events'),
      '#type' => 'fieldset',
    ];
    // Add the role tracking settings.
    $form['group_events']['events'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Trigger a lead update on the following events:'),
      '#default_value' => $config->get('events'),
      '#options' => [
        'login' => $this->t('User login'),
        'create' => $this->t('User registration / creation'),
        'update' => $this->t('User update'),
      ],
    ];

    // Add the validation and submit callbacks.
    $form['#validate'][] = [$this, 'validateForm'];
    $form['#submit'][] = [$this, 'submitForm'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME)
      ->set('events', array_filter($form_state->getValue('events')))
      ->save();
  }

}
