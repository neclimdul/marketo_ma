<?php

namespace Drupal\Tests\marketo_ma_user\Kernel;

use Drupal\Tests\marketo_ma\Kernel\MarketoMaKernelTestBase;

/**
 * Base test for writing Marketo User integration kernel tests.
 *
 * @group marketo_ma_user
 */
abstract class MarketoMaUserKernelTestBase extends MarketoMaKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'marketo_mock_client',
    'marketo_ma_user',
    'marketo_ma',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig('marketo_ma_user');
    $this->installEntitySchema('user');
  }

}
