<?php

namespace Drupal\Tests\marketo_ma_user\Kernel;

use Drupal\marketo_ma_user\Form\FieldMapping;
use Drupal\marketo_ma_user\Form\Settings;
use Drupal\marketo_ma_user\Form\UserActivities;

/**
 * @covers \Drupal\marketo_ma_user\Form\Settings
 * @group marketo_ma_user
 */
class MarketoMaUserSettingsFormTest extends MarketoMaUserKernelTestBase {

  /**
   * Test user config form.
   */
  public function testUserSettingsForm() {
    $marketo_user_settings_form = \Drupal::formBuilder()->getForm(Settings::class);
    $this->assertNotEmpty($marketo_user_settings_form['group_events']['events']);
  }

  /**
   * Test user activities config form.
   */
  public function testUserActivitiesForm() {
    $marketo_user_settings_form = \Drupal::formBuilder()->getForm(UserActivities::class);
    $this->assertNotEmpty($marketo_user_settings_form['enabled_activities']);
    // Tests the rendered form page.
    $content = $this->render($marketo_user_settings_form);
    $this->assertStringContainsString('No activity types, try retrieving from marketo.', $content, 'The empty text is shown for activity types.');
  }

  /**
   * Test user field mapping config form.
   */
  public function testUserFieldMappingForm() {
    $marketo_user_settings_form = \Drupal::formBuilder()->getForm(FieldMapping::class);
    $this->assertNotEmpty($marketo_user_settings_form['mapping']);
    // @todo there should probably be a message if there are no enabled fields.
  }

}
