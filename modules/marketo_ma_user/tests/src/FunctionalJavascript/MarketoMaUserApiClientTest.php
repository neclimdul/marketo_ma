<?php

namespace Drupal\Tests\marketo_ma_user\FunctionalJavascript;

use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_mock_client\TestMarketoMaApiClient;

/**
 * Tests the Marketo MA module in API Client mode.
 *
 * @group marketo_ma_user-js
 */
class MarketoMaUserApiClientTest extends MarketoMaUserJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'filter', 'marketo_mock_client'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config
      ->set('tracking_method', MarketoMaServiceInterface::TRACKING_METHOD_API)
      ->save();
  }

  /**
   * Tests if a lead is associated when the user logs in.
   */
  public function testMunchkinLeadAssociation() {
    $marketo_user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);

    // Log into drupal.
    $this->drupalLogin($marketo_user);

    // Make sure a lead result was returned.
    $leads = \Drupal::service('state')->get(TestMarketoMaApiClient::class);
    $this->assertTrue(!empty($leads), 'A lead was associated ' . var_export($leads, 1));
    // Makes sure the lead email is the same as the user.
    $lead = array_pop($leads);
    $this->assertEquals($lead->getEmail(), $marketo_user->getEmail(), 'Marketo client lead email matches.');
  }

}
