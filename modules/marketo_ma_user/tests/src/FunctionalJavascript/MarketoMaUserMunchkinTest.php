<?php

namespace Drupal\Tests\marketo_ma_user\FunctionalJavascript;

use Drupal\marketo_ma\Service\MarketoMaMunchkinInterface;

/**
 * Tests the Marketo MA module in Munchkin mode.
 *
 * @group marketo_ma_user-js
 */
class MarketoMaUserMunchkinTest extends MarketoMaUserJavascriptTestBase {

  /**
   * Tests if a lead is associated when the user logs in.
   */
  public function testMunchkinLeadAssociation() {
    $marketo_user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);

    // There seems to be a bug where frontpage doesn't get tracking? Fixing that
    // required for this to pass as is.
    $this->drupalGet('<front>');
    $this->assertMunchkinTracking();

    // Log into drupal.
    $this->drupalLogin($marketo_user);
    $this->assertMunchkinTracking(TRUE, [
      [
        'action' => MarketoMaMunchkinInterface::ACTION_ASSOCIATE_LEAD,
        'data' => ['Email' => $marketo_user->getEmail()],
      ],
    ]);

    // Get a tracked page. Should have no more actions.
    $this->drupalGet('/user/password');
    $this->assertMunchkinTracking();
  }

}
