<?php

namespace Drupal\Tests\marketo_ma_user\FunctionalJavascript;

use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;
use Drupal\Tests\marketo_ma\FunctionalJavascript\MarketoMaJavascriptTestBase;
use Drupal\Tests\marketo_ma\Traits\TestFieldsTrait;

/**
 * Base for Marketo MA functional javascript tests.
 *
 * @group marketo_ma-js
 */
abstract class MarketoMaUserJavascriptTestBase extends MarketoMaJavascriptTestBase {

  use TestFieldsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'encryption',
    'marketo_ma',
    'marketo_ma_user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createSampleFields();
    $this->mockConfig();

    \Drupal::configFactory()->getEditable(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME)
      ->set('mapping', ['mail' => 'email'])
      ->save();
  }

}
