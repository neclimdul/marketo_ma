<?php

namespace Drupal\Tests\marketo_ma_user\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base tests for testing user functionality.
 *
 * @group marketo_ma_user
 */
abstract class MarketoMaUserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'marketo_ma', 'marketo_ma_user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser([
      'access all marketo lead data',
      'administer marketo',
    ]);
    $this->drupalLogin($account);
  }

}
