<?php

namespace Drupal\Tests\marketo_ma_contact\Functional;

use Drupal\contact\Entity\ContactForm;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_mock_client\TestMarketoMaApiClient;

/**
 * Tests the marketo_ma_contact admin settings.
 *
 * @group marketo_ma_contact
 * @requires module contact_storage
 *
 * @see \Drupal\marketo_mock_client\TestMarketoMaApiClient
 */
class MarketoMaContactSettingsTest extends MarketoMaContactTestBase {

  /**
   * Test settings form.
   */
  public function testMarketoContactAdminSettingsUi() {
    $account = $this->drupalCreateUser([
      'administer contact forms',
      'administer marketo',
    ]);
    $this->drupalLogin($account);

    $contact_form_id = 'test_contact' . strtolower($this->randomMachineName());
    $edit = [
      'label' => 'test contact',
      'id' => $contact_form_id,
      'recipients' => 'foo@example.com',
      'contact_storage_preview' => FALSE,
    ];
    $this->drupalGet('admin/structure/contact/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('has been added');

    $this->drupalGet("admin/structure/contact/manage/{$contact_form_id}/marketo");
    $this->submitForm([
      'enabled' => 1,
      'mapping[name][mapping]' => 'firstName',
      'mapping[mail][mapping]' => 'email',
    ], 'Save');

    $contact_form = ContactForm::load($contact_form_id);
    $this->assertEquals([
      'name' => 'firstName',
      'mail' => 'email',
    ], $contact_form->getThirdPartySetting('marketo_ma_contact', 'mapping'));
  }

  /**
   * Test contact submit integration.
   */
  public function testContactSubmit() {
    // Talk through the API so we can rely on the test helper.
    \Drupal::configFactory()->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('tracking_method', MarketoMaServiceInterface::TRACKING_METHOD_API)
      ->save();

    /** @var \Drupal\contact\ContactFormInterface $contact_form */
    $contact_form = ContactForm::create([
      'label' => 'test contact',
      'id' => 'test_contact',
      'recipients' => ['foo@example.com'],
      'contact_storage_preview' => FALSE,
    ]);
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'mapping', [
      'name' => 'firstName',
      'mail' => 'email',
    ]);
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'enabled', 1);
    $contact_form->save();

    $account = $this->drupalCreateUser(['access site-wide contact form']);
    $this->drupalLogin($account);
    $this->drupalGet('contact/test_contact');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'subject[0][value]' => 'test subject',
      'message[0][value]' => 'test message',
    ], 'Send message');

    /** @var \Drupal\Core\State\StateInterface $tmp */
    $tmp = $this->container->get('state');
    /** @var \Drupal\marketo_ma\Lead[] $leads */
    $leads = $tmp->get(TestMarketoMaApiClient::class);
    $this->assertEquals($account->getAccountName(), $leads[0]->get('firstName'));
    $this->assertEquals($account->getEmail(), $leads[0]->getEmail());
  }

}
