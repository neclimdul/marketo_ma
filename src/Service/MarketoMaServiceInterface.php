<?php

namespace Drupal\marketo_ma\Service;

use Drupal\marketo_ma\Lead;

/**
 * Service interface for the `marketo_ma` worker service.
 *
 * @todo Should this service has a syncLoad method as well, which would allow us
 *   to send them in bulk via a queue? On top of that there might be more things
 *   we could put into it, like the alter hook, see marketo_ma.api.php.
 */
interface MarketoMaServiceInterface {

  /**
   * Tracking method IDs.
   */
  const TRACKING_METHOD_API = 'api_client';
  const TRACKING_METHOD_MUNCHKIN = 'munchkin';

  /**
   * The Marketo MA config name.
   */
  const MARKETO_MA_CONFIG_NAME = 'marketo_ma.settings';

  /**
   * Gets the marketo_ma config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   *   Marketo config object.
   */
  public function config();

  /**
   * Gets the tracking method from settings.
   *
   * @return string
   *   The tracking method.
   */
  public function trackingMethod();

  /**
   * Adds a given lead to a given list.
   *
   * @param \Drupal\marketo_ma\Lead $lead
   *   The Lead object.
   * @param int $listId
   *   The ID of the list in Marketo.
   *
   * @return $this
   */
  public function addLeadToList(Lead $lead, $listId);

  /**
   * Adds a given e-mail address to a list.
   *
   * @param string $email
   *   The e-mail address of the lead.
   * @param int $listId
   *   The ID of the list in Marketo.
   *
   * @return $this
   */
  public function addLeadToListByEmail($email, $listId);

  /**
   * Updates lead information respecting batch settings.
   *
   * @param \Drupal\marketo_ma\Lead $lead
   *   The Lead object.
   *
   * @return int|null
   *   Lead id of the affected lead.
   */
  public function updateLead(Lead $lead): ?int;

  /**
   * Log a form submission with marketo.
   *
   * @param string $form_id
   *   Marketo Form ID.
   * @param array $fields
   *   Field data to submit to the marketo form.
   * @param string|null $cookie
   *   A cookie to connect with the submission.
   * @param array $extra
   *   Extra parameters to be used for the implementation.
   *
   * @return int|null
   *   Lead id of the affected lead.
   *
   * @see \Drupal\marketo_ma\Service\MarketoMaApiClientInterface::submitForm
   *
   * @throws \Drupal\marketo_ma\Exception\ProcessingException
   *   On failure a relevant exception may be thrown.
   */
  public function postForm(string $form_id, array $fields, ?string $cookie = NULL, array $extra = []): ?int;

  /**
   * Gets fields that are defined in marketo.
   *
   * @return \Drupal\marketo_ma\MarketoFieldDefinition[]
   *   All marketo fields keyed by the marketo field ID.
   */
  public function getMarketoFields();

  /**
   * Retrieves and caches lead fields from Marketo.
   *
   * @return $this
   */
  public function resetMarketoFields();

  /**
   * Gets fields that are read-only in Marketo.
   *
   * @return \Drupal\marketo_ma\MarketoFieldDefinition[]
   *   A list of readonly marketo field IDs.
   */
  public function getReadOnly();

  /**
   * Get's fields that have been enabled.
   *
   * @return \Drupal\marketo_ma\MarketoFieldDefinition[]
   *   All fields available for mapping keyed by marketo field ID.
   */
  public function getEnabledFields();

  /**
   * Gets available fields.
   *
   * @return \Drupal\marketo_ma\MarketoFieldDefinition[]
   *   All fields available for mapping keyed by marketo field ID. If no
   *   fields are enabled, all non-readonly fields are returned.
   */
  public function getAvailableFields();

  /**
   * Determines if the API client is configured and available.
   *
   * @return bool
   *   Whether the API client can connect
   */
  public function apiClientCanConnect();

  /**
   * Get the result from last updateLead call.
   *
   * @return array|null
   *   An array of lead ids and status messages.
   */
  public function getUpdateLeadResult();

}
