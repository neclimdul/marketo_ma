<?php

namespace Drupal\marketo_ma\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\marketo_ma\FieldDefinitionSet;
use Drupal\marketo_ma\Lead;

/**
 * The marketo MA worker service responsible for general module tasks.
 */
class MarketoMaService implements MarketoMaServiceInterface {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The marketo MA API client service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  private $apiClient;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Marketo lead fields.
   *
   * @var \Drupal\marketo_ma\FieldDefinitionSet
   */
  protected $fieldset;

  /**
   * Stores updateLead result.
   *
   * @var array|null
   */
  protected $updateLeadResult;

  /**
   * Creates the Marketo MA core service..
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $api_client
   *   The marketo ma api client.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MarketoMaApiClientInterface $api_client, QueueFactory $queue_factory, ModuleHandlerInterface $moduleHandler) {
    $this->configFactory = $config_factory;
    $this->apiClient = $api_client;
    $this->queueFactory = $queue_factory;
    $this->moduleHandler = $moduleHandler;
    $this->fieldset = new FieldDefinitionSet();
  }

  /**
   * {@inheritdoc}
   */
  public function trackingMethod() {
    return $this->config()->get('tracking_method');
  }

  /**
   * {@inheritdoc}
   */
  public function config() {
    // Use static caching.
    static $config = NULL;
    // Load config if not already loaded.
    if (empty($config)) {
      $config = $this->configFactory->get(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLead(Lead $lead): ?int {
    $this->moduleHandler->alter('marketo_ma_lead', $lead);
    // Get the tracking method.
    if ($formid = $lead->getFormId()) {
      try {
        $lead_id = $this->postForm($formid, $lead->data());
        $this->updateLeadResult = [$lead_id];
        return $lead_id;
      }
      catch (\Exception $e) {
        $this->getLogger('marketo_ma')->warning('Unable to communicate with Marketo: {message}', ['message' => $e->getMessage()]);
      }
    }
    elseif ($this->trackingMethod() === MarketoMaServiceInterface::TRACKING_METHOD_API) {
      // Do we need to batch the lead update?
      if (!$this->config()->get('rest.batch_requests')) {
        // Just sync the lead now.
        return $this->apiClient->syncLead($lead);
      }
      else {
        // Queue up the lead sync.
        $this->queueFactory->get('marketo_ma_lead')->createItem($lead);
      }

      // Move this to an event? Hook? Method specific services?
      \Drupal::service('marketo_ma.page_attachment')->resetUserData();
    }
    else {
      // Set the user data so munchkin can take it from thereqg.
      \Drupal::service('marketo_ma.page_attachment')->setUserData($lead);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function postForm(string $form_id, array $fields, ?string $cookie = NULL, array $extra = []): ?int {
    return $this->apiClient->submitForm($form_id, $fields, $cookie, $extra);
  }

  /**
   * {@inheritdoc}
   */
  public function getMarketoFields() {
    return $this->fieldset->getAll();
  }

  /**
   * {@inheritdoc}
   */
  public function getReadOnly() {
    return $this->fieldset->getReadOnly();
  }

  /**
   * {@inheritdoc}
   */
  public function resetMarketoFields() {
    $api_fields = $this->apiClient->canConnect() ? $this->apiClient->getFields() : [];
    foreach ($api_fields as $api_field) {
      $this->fieldset->add($api_field);
    }
    $this->fieldset->reload();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledFields() {
    return array_intersect_key($this->getMarketoFields(), array_flip($this->config()->get('field.enabled_fields')));
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableFields() {
    $writableFields = $this->fieldset->getWriteable();
    if ($enabledFields = $this->config()->get('field.enabled_fields')) {
      return array_intersect_key($writableFields, array_flip($enabledFields));
    }
    return $writableFields;
  }

  /**
   * {@inheritdoc}
   */
  public function apiClientCanConnect() {
    return $this->apiClient->canConnect();
  }

  /**
   * {@inheritdoc}
   */
  public function getUpdateLeadResult() {
    return $this->updateLeadResult;
  }

  /**
   * {@inheritdoc}
   */
  public function addLeadToList(Lead $lead, $listId) {
    if ($this->trackingMethod() === MarketoMaServiceInterface::TRACKING_METHOD_API) {
      $this->updateLeadResult = $this->apiClient->addLeadsToList($listId, [$lead]);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addLeadToListByEmail($email, $listId) {
    if ($this->trackingMethod() === MarketoMaServiceInterface::TRACKING_METHOD_API) {
      $this->updateLeadResult = $this->apiClient->addLeadToListByEmail($listId, $email);
    }

    return $this;
  }

}
