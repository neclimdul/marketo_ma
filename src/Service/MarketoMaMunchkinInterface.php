<?php

namespace Drupal\marketo_ma\Service;

use Drupal\marketo_ma\Lead;

/**
 * Service interface for the `marketo_ma` munchkin service.
 */
interface MarketoMaMunchkinInterface {

  /**
   * Constants for munchkin javascript actions.
   */
  const ACTION_VISIT_PAGE = 'visitWebPage';
  const ACTION_CLICK_LINK = 'clickLink';
  const ACTION_ASSOCIATE_LEAD = 'associateLead';

  /**
   * Gets the marketo_ma config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   *   The munchkin settings config object.
   */
  public function config();

  /**
   * Determines if all configuration is complete for munchkin integration.
   *
   * @return bool
   *   True if configured, false if parts are missing.
   */
  public function isConfigured();

  /**
   * Gets the Munchkin account ID.
   *
   * @return string
   *   The marketo account id.
   */
  public function getAccountId();

  /**
   * Gets the Munchkin initialization parameters.
   *
   * @return string[]
   *   Hash of any munchkin initialization parameters.
   */
  public function getInitParams();

  /**
   * Munchkin library location.
   *
   * @return string
   *   The url of the munchkin library.
   */
  public function getLibrary();

  /**
   * Get a munchkin action given the action type and lead information.
   *
   * @param string $action_type
   *   The type of action to be preformed.
   *   ('visitWebPage', 'clickLink', 'associateLead')
   * @param \Drupal\marketo_ma\Lead $lead
   *   The lead to be associated. Note: A Lead email is required the
   *   `associateLead` action.
   * @param array $args
   *   Required args for 'visitWebPage' or 'clickLink' actions.
   *
   * @see http://developers.marketo.com/documentation/websites/munchkin-api/
   *
   * @return array
   *   The Drupal settings array required for the action.
   */
  public function getAction($action_type, Lead $lead, array $args = []);

}
