<?php

namespace Drupal\marketo_ma\Service;

use Drupal\marketo_ma\Lead;

/**
 * Service interface for the `marketo_ma` API client.
 *
 * @package Drupal\marketo_ma
 */
interface MarketoMaApiClientInterface {

  /**
   * Checks whether the client has all configuration to connect.
   *
   * @return bool
   *   Returns true of false.
   */
  public function canConnect();

  /**
   * Gets the lead fields that are available for leads (AKA describe).
   *
   * @see: http://developers.marketo.com/documentation/rest/describe/
   *
   * @return array
   *   All the fields available for leads.
   */
  public function getFields();

  /**
   * Gets the activity types fields that are available from marketo.
   *
   * @see: http://developers.marketo.com/documentation/rest/get-activity-types/
   *
   * @return \Drupal\marketo_ma\ActivityType[]
   *   All activity types.
   */
  public function getActivityTypes();

  /**
   * Retrieves lead information.
   *
   * @param string $email
   *   The leads email address.
   *
   * @return \Drupal\marketo_ma\Lead|null
   *   The lead.
   */
  public function getLeadByEmail($email);

  /**
   * Retrieves lead information.
   *
   * @param int $id
   *   The leads marketo id.
   *
   * @return \Drupal\marketo_ma\Lead
   *   The lead.
   */
  public function getLeadById($id);

  /**
   * Retrieves lead activity information.
   *
   * @param \Drupal\marketo_ma\Lead $lead
   *   The lead.
   * @param array $activity_type_ids
   *   Activity type ids to be viewed.
   */
  public function getLeadActivity(Lead $lead, array $activity_type_ids = []);

  /**
   * Inserts or updates a lead.
   *
   * @param \Drupal\marketo_ma\Lead $lead
   *   The lead to be updated.
   * @param string $key
   *   Lead Key, typically email address.
   *
   * @return int|null
   *   Lead id of the affected lead. Null on failure or queue.
   */
  public function syncLead(Lead $lead, $key = 'email'): ?int;

  /**
   * Delete one or more leads.
   *
   * @param int|int[] $leads
   *   Either a single lead ID or an array of lead IDs.
   *
   * @return \Drupal\marketo_ma\Lead[]|null
   *   An array of response messages and ids. Null on failure.
   */
  public function deleteLead($leads);

  /**
   * Adds an e-mail address to a given list.
   *
   * @param int $listId
   *   The ID of the target list. The List Id can be obtained from the URL of
   *   the list in the UI, where the URL will resemble
   *   https://app-***.marketo.com/#ST1001A1. In this URL, the id is 1001, it
   *   will always be between the first set of letters in the URL and the
   *   second set of letters.
   * @param string $email
   *   The email address of the user that needs to be added to the list.
   * @param array $options
   *   Array of additional options to configure lead syncing.
   *
   * @return array|null
   *   An array of response messages (errors) or NULL if the transaction was
   *   successful.
   */
  public function addLeadToListByEmail($listId, $email, array $options = []);

  /**
   * Adds a given set of leads to a target static list.
   *
   * @param int $listId
   *   The ID of the target list.
   * @param \Drupal\marketo_ma\Lead[] $leads
   *   List of leads to add to a list.
   * @param array $options
   *   Array of additional options to configure lead syncing.
   *
   * @return array|null
   *   An array of response messages (errors) or NULL if the transaction was
   *   successful.
   */
  public function addLeadsToList($listId, array $leads, array $options = []);

  /**
   * Log a form submission with marketo.
   *
   * Extra parameters:
   *  Key        | Rest API field      | Notes
   *  -----------|---------------------|-------------
   *  referer    | pageUrl             | Attach a referer url.
   *  query      | queryString         | Separate url query strings.
   *  ip_address | leadClientIpAddress | The ip address us the form submitter.
   *  user_agent | userAgentString     | The user agent string of the submitter.
   *
   * @param string $form_id
   *   Marketo Form ID.
   * @param array $fields
   *   Form fields.
   * @param string|null $cookie
   *   A cookie to connect with the submission.
   * @param array $extra
   *   Extra parameters to be used for the implementation.
   *
   * @return int
   *   Lead id of the affected lead.
   *
   * @throws \Drupal\marketo_ma\Exception\ProcessingException
   *   On failure a relevant exception may be thrown.
   */
  public function submitForm(string $form_id, array $fields, ?string $cookie = NULL, array $extra = []);

}
