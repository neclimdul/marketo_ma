<?php

namespace Drupal\marketo_ma\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field management form.
 */
class MarketoMaFieldMgmt extends ConfigFormBase {

  /**
   * The Marketo MA core service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $service;

  /**
   * Constructs a \Drupal\marketo_ma\Form\MarketoMASettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $service
   *   The marketo ma service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MarketoMaServiceInterface $service
  ) {
    parent::__construct($config_factory);
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('marketo_ma')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_field_mapping';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var array $form */
    $form = parent::buildForm($form, $form_state);

    $form['description'] = [
      '#markup' =>
        '<p>' .
        $this->t(
          'By default, all fields below will be available for mapping to Webform and User Profile fields. It is possible to limit the available fields by selecting them below. Read-only fields are displayed here but are never available for mapping.'
        ) .
        '</p>',
    ];

    // Get the configuration.
    $config = $this->config(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);

    // Force refresh from Marketo.
    $trigger = $form_state->getTriggeringElement();
    if (!is_null($trigger) && in_array('field_api_retrieve_fields', $trigger['#array_parents'])) {
      $this->service->resetMarketoFields();
    }
    $options = [];
    foreach ($this->service->getMarketoFields() as $field_value) {
      $options[$field_value->getRestName()] = [
        'displayName' => $field_value->getDisplayName(),
        'restName' => $field_value->getRestName(),
        'soapName' => $field_value->getSoapName(),
      ];
    }

    $form['field_enabled_fields'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Marketo fields'),
      '#header' => [
        'displayName' => $this->t('Display Name'),
        'restName' => $this->t('REST Field'),
        'soapName' => $this->t('SOAP/Munchkin Field'),
      ],
      '#options' => $options,
      '#empty' => $this->t('No fields, try retrieving from Marketo.'),
      '#prefix' => '<div id="marketo-defined-fields-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => array_fill_keys($config->get('field.enabled_fields'), 1),
    ];

    foreach ($this->service->getReadOnly() as $field_key => $field) {
      $form['field_enabled_fields'][$field_key]['#disabled'] = TRUE;
    }

    // Add the ajax button that get's fields from the marketo API.
    $form['actions']['field_api_retrieve_fields'] = [
      '#type' => 'button',
      '#value' => $this->t('Retrieve from Marketo'),
      '#disabled' => !$this->service->apiClientCanConnect(),
      '#ajax' => [
        'callback' => [$this, 'retrieveApiFields'],
        'event' => 'mouseup',
        'wrapper' => 'marketo-defined-fields-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Retrieving fields from Marketo...'),
        ],
      ],
    ];

    return $form;
  }

  /**
   * Connects to Marketo and retrieves the API fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form element to replace in the ajax wrapper setting.
   */
  public function retrieveApiFields(array &$form, FormStateInterface $form_state) {
    // Return the form element that will bre replaced in the wrapper element.
    return $form['field_enabled_fields'];
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('field.enabled_fields', array_filter($form_state->getValue('field_enabled_fields')))
      ->save();
  }

}
