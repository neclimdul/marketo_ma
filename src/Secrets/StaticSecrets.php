<?php

namespace Drupal\marketo_ma\Secrets;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Naive "secrets" implementation that just accepts values to its constructor.
 */
class StaticSecrets implements SecretsInterface {

  use StringTranslationTrait;

  /**
   * A list of our secrets.
   *
   * @var array
   */
  protected $secrets;

  /**
   * Construct a StaticSecrets object.
   *
   * @param array $secrets
   *   A list of our secrets.
   */
  public function __construct(array $secrets) {
    $this->secrets = $secrets;
  }

  /**
   * {@inheritDoc}
   */
  public function getClientId() {
    return $this->secrets['rest.client_id'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getClientSecret() {
    return $this->secrets['rest.client_secret'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getMunchkinApiKey() {
    return $this->secrets['munchkin.api_private_key'] ?? '';
  }

}
