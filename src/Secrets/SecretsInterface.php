<?php

namespace Drupal\marketo_ma\Secrets;

/**
 * A interface for objects exposing secrets to marketo.
 */
interface SecretsInterface {

  /**
   * Get the Marketo REST client ID.
   *
   * @return string
   *   The Marketo REST client ID.
   */
  public function getClientId();

  /**
   * Get the Marketo REST client secret.
   *
   * @return string
   *   The Marketo REST client secret.
   */
  public function getClientSecret();

  /**
   * Get the Marketo Munchkin API key.
   *
   * @return string
   *   The munchkin API key.
   */
  public function getMunchkinApiKey();

}
