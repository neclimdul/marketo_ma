<?php

namespace Drupal\marketo_ma;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Represents a Marketo Field.
 */
class MarketoFieldDefinition {

  use StringTranslationTrait;

  /**
   * The lead metadata.
   *
   * @var array
   */
  protected $definition;

  /**
   * MarketoField constructor.
   *
   * @param array $definition
   *   The Marketo field data.
   */
  public function __construct(array $definition = []) {
    $this->definition = $definition;
  }

  /**
   * Get the Marketo Field ID.
   *
   * @return string
   *   The Field ID.
   */
  public function id() {
    return $this->definition['id'];
  }

  /**
   * Gets the display name for the Marketo MA field.
   *
   * @return string
   *   The display name.
   */
  public function getDisplayName() {
    return $this->definition['displayName'];
  }

  /**
   * Get the allowed field length.
   *
   * @return int
   *   The allowed field length.
   */
  public function getLength() {
    return $this->definition['length'];
  }

  /**
   * Get the name used for interacting with the REST API.
   *
   * @return string
   *   The REST name.
   */
  public function getRestName() {
    return $this->definition['restName'];
  }

  /**
   * If the field is read only in the REST API.
   *
   * @return int
   *   1 if read only, 0 if writeable.
   */
  public function getRestReadOnly() {
    return $this->definition['restReadOnly'];
  }

  /**
   * Get the name used for interacting mostly with the Munchkin API.
   *
   * @return string
   *   The SOAP/Munchkin name.
   */
  public function getSoapName() {
    return $this->definition['soapName'];
  }

  /**
   * If the field is read only in the SOAP and Munchkin interfaces.
   *
   * @return int
   *   1 if read only, 0 if writeable.
   */
  public function getSoapReadOnly() {
    return $this->definition['soapReadOnly'];
  }

  /**
   * Get the field name for a given api type.
   *
   * @param string $tracking_method
   *   The source api for which the field name is being requested.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName($tracking_method) {
    switch ($tracking_method) {
      case MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN:
        return $this->getSoapName();

      case MarketoMaServiceInterface::TRACKING_METHOD_API:
        return $this->getRestName();
    }

    throw new \InvalidArgumentException('Unknown tracking method.');
  }

}
