<?php

namespace Drupal\marketo_ma\Rest;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\marketo_ma\Secrets\SecretsInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use NecLimDul\MarketoRest\Configuration;

/**
 * API configuration setup from Drupal configuration.
 */
class Config extends Configuration {

  use ConfigTrait;

  /**
   * Construct a rest library configuration object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Drupal\marketo_ma\Secrets\SecretsInterface $secrets
   *   Marketo secrets.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SecretsInterface $secrets) {
    $config = $config_factory->get(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
    $defaults = [
      'baseUrl' => $this->getMarketoBaseUrl($config),
      'clientId' => $secrets->getClientId(),
      'clientSecret' => $secrets->getClientSecret(),
    ];
    parent::__construct($defaults);
  }

}
