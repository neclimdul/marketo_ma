<?php

/**
 * @file
 * Post update hooks for Marketo MA module.
 */

use Drupal\marketo_ma\FieldDefinitionSet;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Migrate enabled fields to api name.
 */
function marketo_ma_post_update_convert_enabled_to_list() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
  $data = $config->getRawData();
  $new_data = [];
  $old_data = $data['field']['enabled_fields'];
  $definitions = new FieldDefinitionSet();
  foreach ($definitions->getAll() as $definition) {
    if (isset($old_data[$definition->id()])) {
      $new_data[] = $definition->getRestName();
    }
  }

  $data['field']['enabled_fields'] = $new_data;
  $config->setData($data)->save();
}
